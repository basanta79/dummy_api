from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
from handlers.authhandler import AuthHandler

def make_app():
  urls = [("/api/auth/", AuthHandler)]
  return Application(urls)
  
if __name__ == '__main__':
    app = make_app()
    app.listen(3001)
    IOLoop.instance().start()
